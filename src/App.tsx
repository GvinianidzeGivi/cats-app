import React, { useEffect } from "react";
import Sidebar from "./components/sidebar/sidebar.component";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import routes from "./routes";
import { useDispatch, useSelector } from "react-redux";
import Layout from "./components/Layout";
import { getCategories, getCats } from "./redux/ducks/catsReducer";

import { RootState } from "./redux/store";

const RenderRoute = (route: any) => {
  return (
    <Route
      path={route.path}
      exact={route.exect}
      render={(props) => (
        <Layout title={route.title}>
          <route.component {...props} />
        </Layout>
      )}
    ></Route>
  );
};

const App: React.FC = () => {
  const dispatch = useDispatch();
  const { categories, selectedCategoryId, pageLimit } = useSelector(
    (state: RootState) => state.cats
  );

  useEffect(() => {
    dispatch(getCategories());
  }, [dispatch]);

  useEffect(() => {
    if (selectedCategoryId) {
      dispatch(getCats());
    }
  }, [dispatch, pageLimit, selectedCategoryId]);

  return (
    <div className="App">
      <Router>
        <div style={{display: 'flex'}}>
        <Sidebar categories={categories} />
        <Switch>
          {routes.map((route, index) => (
            <RenderRoute {...route} key={index} />
          ))}
        </Switch>
        </div>
      </Router>
    </div>
  );
};

export default App;
