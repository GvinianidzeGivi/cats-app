import React, { useEffect } from "react";

const Layout: React.FC<any> = ({ title, children }) => {
  useEffect(() => {
    document.title = title;
  }, [title]);
  return children;
}
export default Layout;
