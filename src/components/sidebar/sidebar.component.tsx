import React from "react";
import { Link } from "react-router-dom";
import { Category } from "../../domain/models";


type SidebarProps = {
  categories: Category[];
};

const SidebarComponent: React.FC<SidebarProps> = (props: SidebarProps) => {
  return (
    <div className="sidebar">
      <ul className="sidebar-list">
        {props.categories.map((category) => {
          return (
            <li  className="sidebar-list__item" key={category.id}>
              <Link  className="sidebar-list__link" to={`${category.id}`}>{category.name}</Link>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default SidebarComponent;
