import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import styled from "styled-components";
import { useParams } from "react-router-dom";
import {
  increasePageLimitBy,
  selectCategoryId,
} from "../redux/ducks/catsReducer";
import { RootState } from "../redux/store";
import ButtonComponent from "../components/button/button.component";

interface CatListType {
  title: string
}

const Loading = styled.div`
  font-size: 3em;
`;

const Category = styled.h1`
  color: lightcoral;
`;

const CatList: React.FC<CatListType> = ({title}) => {
  const { id }: any = useParams();
  const dispatch = useDispatch();
  const results = useSelector((state: RootState) => state.cats.results);

  const category = useSelector((state: RootState) =>
    state.cats.categories.find((category) => category.id === +id)
  );

  useEffect(() => {
    dispatch(selectCategoryId(id));
    if(category) document.title = category.name;
  }, [id, category, dispatch, title]);

  return (
    <div style={{border: "2px solid black"}}>
      {results.length ? (
        <>
          <Category>{category?.name ?? ""}</Category>
          <div className="cats">
            
          {results.map((cat, index) => (
            <div key={`${cat.id}${index}`} className="cats__thumb">
              <img className="cats__image" src={cat.url}  alt="Cat" />
            </div>
          ))}
          </div>

          <ButtonComponent loadMore={() => dispatch(increasePageLimitBy(10))}>
            Load more
          </ButtonComponent>
        </>
      ) : (
        <Loading>Loading..</Loading>
      )}
    </div>
  );
}

export default CatList;
