import React from 'react';  
import styled from "styled-components";

const Landing = styled.div`
  font-size: 3em;
`;

const LandingPage: React.FC = () => {
  return <Landing>Cat's App</Landing>;
}

export default LandingPage;
