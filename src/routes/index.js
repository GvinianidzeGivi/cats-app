import CatList from "../containers/CatList";
import LandingPage from "../containers/LandingPage";
const routes = [
  {
    path: "/:id",
    component: CatList,
    title: "Cats",
    exect: false,
  },
  {
    path: "/",
    component: LandingPage,
    title: "Landing Page",
    exect: true,
  },
];

export default routes;
